FROM alpine:latest

RUN apk update
RUN apk add ansible openssh-client

RUN mkdir ~/.ssh
